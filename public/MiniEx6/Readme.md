MiniEx6

![ScreenShot](screenshot.png)

My sketch: https://stinemygind.gitlab.io/ap2020/MiniEx6

Runme: https://gitlab.com/StineMygind/ap2020/-/blob/master/public/MiniEx6/RUNMEex6.js

For this miniEx my goal was to understand how classes work and what you can use them for. I have been trying to make a simple game, which I think was very difficult, even though it doesn’t look like it. I used Winnie's program as an inspiration because I thought it would be too difficult for me to make a game on my own.

I have made a game where the player is supposed to catch the green balls falling from the top of the screen and at the same time try to avoid the red balls. The player is using a rectangle controlled by the left and right arrow key to catch the balls. The player has five lives, which is referring to how many times the player catches a red ball. The game stops if you have no lives left or you miss more than 50 green balls. 

I have been using two classes to make red and green balls, which has been saving me a lot of time because the classes make it possible to create a large number of objects that has the same movement and look. The classes are very simple because they just consist of ellipses that are moving downwards by adding 1 or 2 to the y-positions. It makes the balls move with different speeds and they also spread randomly in width.
I have also made different kinds of functions to check if the balls are getting caught or not, and how many balls are shown on the screen. A thing that I have been struggling a lot with is using “dist” to tell if the balls got caught or not. I never succeeded with it, and I used another method instead, where an if-statement could decide whether the balls got caught.

As I mentioned before, I have made some simple classes, by deciding what they should consist of, and how many characteristics I want to take into account. This is very important when applying a concept from the real world into the world of computation. The ball, that the class creates, is an abstraction because I have chosen to leave some of the characteristics of a ball out. I could, for instance, have chosen to make the ball in 3D, but I didn’t. When programming the programmer have to decide which characteristics should be applied and which should be left out. This is making the programmer very powerful, because he is in a position where he has to characterize different things, and this could, for example, be a human, which I think is pretty difficult to characterize without excluding someone. So the programmer carries a responsibility when deciding these things, and I think that is relevant for us on Digital Design because we learn how to create user-friendly solutions and user experiences, which could be depending on how good the programmer is at object-oriented programming.

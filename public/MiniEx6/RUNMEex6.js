//defining variables
let ball = []; //array of green balls
let min_ball = 10; //the minimum of balls on the screen
let redball = []; //array of red balls
let min_redball = 10; //the minimum of red balls on the screen
let rectPosX; //the x position of the rectangle
let rectSize = { //the size of the rectangle
  w:50,
  h:30
};
let score =0; //how many green balls have been caught
let lose = 0; //how many green balls have been missed
let lives = 5; //how many red balls have been caught

function setup() {
  createCanvas(710, 400);

  rectPosX = width/2; //starting at the centre of the canvas

  for (let i=0; i<=min_ball; i++) {
    ball[i] = new Ball(); //creating the green balls
}

  for (let i=0; i<=min_redball; i++) {
    redball[i] = new RedBall(); //creating the red balls
}
}

function draw() {
  background(0);

//creating the rectangle
  fill('#fae')
  rect(rectPosX,370,rectSize.w,rectSize.h);

//Calling functions
  displayScore();
  checkBallNum();
  showRedBall();
  showBall();
  checkRedBallNum();
  checkCatch();
  checkRedCatch();
  gameOver();

}

function checkCatch(){ //checking if the ball is getting catched by the rectangle
  for (let i =0;i<ball.length;i++){
    if (ball[i].pos.y>360 && ball[i].pos.y<400){ // if the ball's y-position is within 360 and 400
    if(ball[i].pos.x>rectPosX && ball[i].pos.x<(rectPosX+rectSize.w)){ //if the ball's x-position is within the rectangle
      score++; //if the ball gets catched, add 1 to the score
      ball.splice(i,1); //add 1 to the array
}
}
    if(ball[i].pos.y >= 399){
      lose++; //the ball doesn't get catched, add 1 to the wasted balls
}
}
}

function checkRedCatch(){ //checking if the ball is getting catched by the rectangle
  for (let i =0;i<redball.length;i++){
    if (redball[i].pos.y>360 && redball[i].pos.y<400){ // if the ball's y-position is within 360 and 400
    if(redball[i].pos.x>rectPosX && redball[i].pos.x<(rectPosX+rectSize.w)){ //if the ball's x-position is within the rectangle
      lives--; //if the ball gets catched, add -1 to the to the lives
      redball.splice(i,1); //add 1 to the array
}
}
}
}

function gameOver(){
  if (lives<=0 || lose==51){ //you loose if you have no lives left or you have missed 50 greenb balls
    noLoop();
    stroke('#fae');
    strokeWeight(4);
    fill(255);
    textSize(50);
    text('GAME OVER ', width/4, height/2);
    text('You caught '+ score + " green ball(s)", 60, height/2+50);
}
}


function displayScore() {
  fill(255);
  textSize(17);
  text('You have '+ score + " green ball(s)", 10, height/2-180);
  text('You have wasted ' + lose + " ball(s)", 10, height/2-160);
  text('You have ' + lives + " lives left", 10, height/2-140);
}

function keyPressed() { //the x-position of the rectangle is changing by +/-10 when you use the left and right arrow
  if (keyCode === LEFT_ARROW) {
    rectPosX-=10;
  } else if (keyCode === RIGHT_ARROW) {
    rectPosX+=10;
}
}

function showRedBall(){ //red balls falling
  for (let i = 0; i <redball.length; i++) {
    redball[i].move();
    redball[i].show();
  if(redball[i].pos.y>height){ //if the ball dissapears from the screen
      redball.splice(i,1); //add 1 to the array
}
}
}

function showBall(){ //green balls falling
for (let i = 0; i <ball.length; i++) {
  ball[i].move();
  ball[i].show();
  if(ball[i].pos.y>height){ //if the ball dissapears from the screen
    ball.splice(i,1); //add 1 to the array

}
}
}

function checkBallNum() {
  if (ball.length < min_ball) //checking how many balls are on the screen
  ball.push(new Ball()); //adding ball to the array
}

function checkRedBallNum() {
  if (redball.length < min_redball) //checking how many balls are on the screen
  redball.push(new RedBall()); //adding ball to the array
}


class Ball { //creating a class for the green balls
  constructor() {
    this.pos = new createVector(random(10,width-10));
    this.diameter = (20);
    this.speed = random(1,2);
}

  move() {
    this.pos.y+=this.speed;
}

  show() {
    fill (64, 228, 64);
    ellipse(this.pos.x, this.pos.y, this.diameter, this.diameter);
}
}

class RedBall { //creating a class for the red balls
  constructor() {
    this.pos = new createVector(random(10,width-10));
    this.diameter = (20);
    this.speed = random(1,2);
}

  move() {
    this.pos.y+=this.speed;
}

  show() {
    fill (249, 7, 33);
    ellipse(this.pos.x, this.pos.y, this.diameter, this.diameter);

}
}

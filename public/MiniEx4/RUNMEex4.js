//Defining sliders
let slider;
let slider2;
let slider3;
let slider4;
let slider5;
let slider6;
let slider7;
let slider8;
let slider9;
let slider10;
let slider11;
let slider12;
let slider13;
let slider14;
let slider15;
let slider16;
let slider17;

function setup() {
createCanvas(900,500)
  background(255);


  //Slider 1
  slider = createSlider(0, 255);
  slider.position(740, 20);
  slider.style('width', '80px');

  //Slider 2
  slider2 = createSlider(0, 255);
  slider2.position(740, 40);
  slider2.style('width', '80px');

  //Slider 3
  slider3 = createSlider(0, 255);
  slider3.position(740, 60);
  slider3.style('width', '80px');

  //Slider 4
  slider4 = createSlider(0, 255);
  slider4.position(740, 80);
  slider4.style('width', '80px');

  //Slider 5
  slider5 = createSlider(0, 255);
  slider5.position(740, 100);
  slider5.style('width', '80px');

  //Slider 6
  slider6 = createSlider(0, 255);
  slider6.position(740, 120);
  slider6.style('width', '80px');

  //Slider 7
  slider7 = createSlider(0, 255);
  slider7.position(740, 140);
  slider7.style('width', '80px');

  //Slider 8
  slider8 = createSlider(0, 255);
  slider8.position(740, 160);
  slider8.style('width', '80px');

  //Slider 9
  slider9 = createSlider(0, 255);
  slider9.position(740, 180);
  slider9.style('width', '80px');

  //Slider 10
  slider10 = createSlider(0, 255);
  slider10.position(740, 200);
  slider10.style('width', '80px');

  //Slider 11
  slider11 = createSlider(0, 255);
  slider11.position(740, 220);
  slider11.style('width', '80px');

  //Slider 12
  slider12 = createSlider(0, 255);
  slider12.position(740, 240);
  slider12.style('width', '80px');

  //Slider 13
  slider13 = createSlider(0, 255);
  slider13.position(740, 260);
  slider13.style('width', '80px');

  //Slider 14
  slider14 = createSlider(0, 255);
  slider14.position(740, 280);
  slider14.style('width', '80px');

  //Slider 15
  slider15 = createSlider(0, 255);
  slider15.position(740, 300);
  slider15.style('width', '80px');

  //Slider 16
  slider16 = createSlider(0, 255);
  slider16.position(740, 320);
  slider16.style('width', '80px');

  //Slider 17
  slider17 = createSlider(0, 255);
  slider17.position(740, 340);
  slider17.style('width', '80px');
}

function draw() {
background(255);
noStroke ();

 let val = slider.value();
 let val2 = slider2.value();
 let val3 = slider3.value();
 let val4 = slider4.value();
 let val5 = slider5.value();
 let val6 = slider6.value();
 let val7 = slider7.value();
 let val8 = slider8.value();
 let val9 = slider9.value();
 let val10 = slider10.value();
 let val11 = slider11.value();
 let val12 = slider12.value();
 let val13 = slider13.value();
 let val14 = slider14.value();
 let val15 = slider15.value();
 let val16 = slider16.value();
 let val17 = slider17.value();

fill(val);
rect(0,0,100,200);

fill(val2);
rect(100,100,100,200);

fill(val3);
rect(0,200,100,100);

fill(val4);
rect(0,300,200,200);

fill(val5);
ellipse(100,400,val5,val5);

fill(val6);
triangle(100, 100, 100, 0, 200, 100);

fill(val7);
triangle(200, 0, 100, 0, 200, 100);

fill(val8);
rect(200,0,300,100);

fill(val9);
rect(200,100,200,200);

fill(val10);
triangle(200, 500, 200, 300, 400, 500);

fill(val11);
triangle(400, 300, 200, 300, 400, 500);

fill(val12);
rect(400,100,100,400);

fill(val13);
rect(500,0,200,200);

fill(val14);
ellipse(600,100,val14,val14);

fill(val15);
rect(500,400,200,100);

fill(val16);
triangle(500, 200, 700, 200, 500, 400);

fill(val17);
triangle(700, 400, 700, 200, 500, 400);


}

function setup() {
    createCanvas(400, 400);
    //calling function to draw the first four lines
    makeNewA();
    makeNewB();
    makeNewC();
    makeNewD();
    background(255);
    }

function draw() {
  push();
    ellipses();
  pop();

  //lines from top to bottom
  a.move(); //from the class
  a.draw(); //from the class
  if (a.isOutside()) { //if the line reaches the opposite side --> make new line
    makeNewA()
  }

  //lines from bottom to top
  b.move(); //from the class
  b.draw(); //from the class
  if (b.isOutside()) { //if the line reaches the opposite side --> make new line
    makeNewB()
  }

  //lines from left to right
  c.move(); //from the class
  c.draw(); //from the class
  if (c.isOutside()) { //if the line reaches the opposite side --> make new line
    makeNewC()
  }

  //lines from right to left
  d.move(); //from the class
  d.draw(); //from the class
  if (d.isOutside()) { //if the line reaches the opposite side --> make new line
    makeNewD()
  }
}

function makeNewA() {
    randomX = random(0, 400);
    a = new A(randomX, 0); //position of new lines
  }

function makeNewB() {
    randomX = random(0, 400);
    b = new B(randomX, 400); //position of new lines
  }

function makeNewC() {
    randomY = random(0, 400);
    c = new C(0, randomY); //position of new lines
  }

function makeNewD() {
    randomY = random(0, 400);
    d = new D(400, randomY); //position of new lines
  }


class A { //lines from top to bottom
  constructor(x, y) { //arguments
    this.x = x;
    this.y = y;
    let color = ['#56F6CF', '#2AB2D4', '#3FCBC5', '#638CF9' ]; //array of colours
    this.color = (random(color));
  }
  isOutside() { //function to find out if the ellipse is outside the canvas
    return this.x > 400 || this.x < 0 || this.y > 400 || this.y < 0;
  }
  move() {
      this.y += 2; //making the ellipse move
  }
  draw() {
    noStroke();
    fill(this.color);
    ellipse(this.x, this.y, 20);
  }
}

class B {  //lines from bottom to top
  constructor(x, y) { //arguments
    this.x = x;
    this.y = y;
    let color = ['#56F6E0', '#56D9F6', '#56A1F6', '#3FCBC5']; //array of colours
    this.color = random(color);
  }
  isOutside() { //function to find out if the ellipse is outside the canvas
    return this.x > 400 || this.x < 0 || this.y > 400 || this.y < 0;
  }
  move() {
      this.y -= 2; //making the ellipse move
  }
  draw() {
    noStroke();
    fill(this.color);
    ellipse(this.x, this.y, 20);
  }
}

class C { //lines from left to right
  constructor(x, y) { //arguments
    this.x = x;
    this.y = y;
    let color = ['#3C30EC', '#5B72F0', '#25ABCB ', '#56D9F6']; //array of colours
    this.color = random(color);
  }
  isOutside() { //function to find out if the ellipse is outside the canvas
    return this.x > 400 || this.x < 0 || this.y > 400 || this.y < 0;
  }
  move() {
      this.x += 2; //making the ellipse move
  }
  draw() {
    noStroke();
    fill(this.color);
    ellipse(this.x, this.y, 20);
  }
}

class D { //lines from right to left
  constructor(x, y) { //arguments
    this.x = x;
    this.y = y;
    let color = ['#06CCFD', '#14CFE2', '#75EBF7', '#3C30EC']; //array of colours
    this.color = random(color);
  }
  isOutside() { //function to find out if the ellipse is outside the canvas
    return this.x > 400 || this.x < 0 || this.y > 400 || this.y < 0;
  }
  move() {
      this.x -= 2; //making the ellipse move
  }
  draw() {
    noStroke();
    fill(this.color);
    ellipse(this.x, this.y, 20);
  }
}

function ellipses(){ //ellipse in the middle
  push();
    translate(200, 200);
    rotate(radians(frameCount)); //making it rotate
    for (var i = 0; i < 1; i++) { //for-loop
  push();
    fill(249, 51, 141)
    stroke(249, 51, 141);
    strokeWeight(3)
    ellipse(50, 50, 10, 10);
  pop();
}
}

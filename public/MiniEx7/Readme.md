MiniEx7

![ScreenShot](screenshot.png)

My sketch: https://stinemygind.gitlab.io/ap2020/MiniEx7

Runme: https://gitlab.com/StineMygind/ap2020/-/blob/master/public/MiniEx7/RUNMEex7.js

What are the rules in your generative program and describe how your program performs over time and how are the running of the rules contingently enabled emergent behaviours?

For this miniEx I started with a blank piece of paper and tried to draw some basic lines, to figure out what the rules of my program should be. I decided that I wanted basic lines to be drawn in my program, and they should start from one of the sides four sides of the canvard and move to the opposite side. I wanted them to have random colours and a random starting point. I have made four different classes for each of the four lines, which is starting from the four sides of the canvas. I have made a for-loop in the middle of the canvas, where an ellipse is circling again and again, which means that it is drawing on the existing lines. 
To be honest, this doesn’t look very impressive, but I have used a lot of time on understanding how I could make good use of classes and how I could arrange my code in s practical way. First I had just one class but I couldn’t make it work, so I made four, one for each line, instead. 

What's the role of rules and processes in your work?

So my rules are, that four ellipses are drawing a line from each side of the canvas, and every time they reach the opposite side of the canvas, a new line is drawn. Each colour is picked randomly between four different blue scaled colours, which I have put in an array. The position of the lines on the sides of the canvas is also picked randomly.

Draw upon the assigned reading(s), how does this mini-exercise help you to understand auto generator (e.g control, autonomy, instructions/rules)? Do you have any further thoughts about the theme of this chapter?

I think that this miniEx has helped me to understand how to create something auto generative. It is kind of a new way to code because you have to think more about instructions and make it do the same things (maybe in a new way) over and over again. I think it is quite interesting to explore this way of coding, and I like that we have also had some requirements for this miniEx. 
My thoughts on generative programs are that they are relevant to examine and learn how to make, but I’m not really sure when or how they are being used in the real world. Maybe I’m blinded by the examples from the assigned video and because of that I can’t imagine it. Speaking of the assigned video, we have seen that generative programs are used as art, and I think that it is an interesting way of using software to generate art. Usually, the generative art also has an aspect of randomness, which is creating a unique specimen each time the program is drawn. If taking the term randomness into account it is interesting how randomness is used in software art, because I usually think of art as something that consists of many thoughtful considerations. But now I see it in a new way, where art also is the unknown part and new possibilities or patterns can appear.   

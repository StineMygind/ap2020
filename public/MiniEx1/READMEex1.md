README
![ScreenShot](screenshot.png)


I find my first independent coding experience very difficult and confusing. I have never been coding before, so I felt like I missed some basic information like “;” after every sentence. I had a lot of difficulties with coding, because my program often disappeared on the live server, and I didn’t understand why. I got help from the references, where I was able to see basic syntax and how to place it in the correct order. It was easy to copy it and place it in my own code, though it sometimes ruined everything else. So I also had to be careful with copying the references, because they often contained “function setup” which I already had done.
Even though I had a lot of trouble, I enjoyed it and I had a lot of fun trying things out. And when my coding sometimes turned out as a success I felt very happy. The most difficult thing, was to make things move, and I used a lot of time on making the sunbeams rotate, but I never succeeded with that. I also think it took a lot of time to write the correct coordinates, so the stuff was in the exact place that I wanted.
The coding process is very different from reading and writing. For me, it takes a lot more to understand and I have to look closely at all signs, to understand what is going on. You also have to be very precise when coding, because misplaced letters or missing signs can make the output very different or simply collapse.
I don’t really know what programming means to me. I think it has influenced my life a lot, and I look up to people who master a discipline like programming and coding.
The readings made me think of which impact programming has had on the whole world. It’s almost everywhere in our everyday life, and that is also a bit scaring.

https://stinemygind.gitlab.io/ap2020/MiniEx1

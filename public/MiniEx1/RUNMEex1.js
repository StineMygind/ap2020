var x=0;
var y=0;
function setup() {
  createCanvas(640, 480);
  background (0, 100, 300);
}
function draw() {
x=x+1;
y=y+1;
  stroke('rgb(0,255,0)');
  strokeWeight(4);
  line (400, 300, 400, 480);

  stroke(255, 204, 0);
  strokeWeight(4);
  fill(255, 204, 0);
  circle(90, 90, 100);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (35, 35, 0, 0);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (60, 20, 50, 0);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (90, 15, 90, 0);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (120, 20, 130, 0);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (150, 30, 170, 0);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (170, 60, 240+x, 15-1/2*y);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (170, 90, 250+x, 90);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (165, 120, 240+3*x, 140+y);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (145, 150, 220+x, 200+1/2*y);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (120, 165, 150+1/2*x, 240+y);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (90, 170, 90, 250+y);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (55, 165, 30-x, 240+2*y);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (30, 150, 0, 180);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (20, 120, 0, 125);

  stroke(255, 204, 0);
  strokeWeight(4);
  line (20, 85, 0, 75);

  noStroke();
  fill('red');
  circle(445, 280, 50);

  noStroke();
  fill('red');
  circle(435, 250, 50);

  noStroke();
  fill('red');
  circle(405, 235, 50);

  noStroke();
  fill('red');
  circle(370, 250, 50);

  noStroke();
  fill('red');
  circle(360, 290, 50);

  noStroke();
  fill('red');
  circle(380, 320, 50);

  noStroke();
  fill('red');
  circle(425, 315, 50);

  noStroke();
  fill('#fae');
  circle(400, 280, 50);

  noStroke();
  fill(150,100,100);
  triangle(0, 400, 100, 400, 50, 270);

  fill(200,200,200);
  circle(50, 250, 60);

  stroke(0, 0, 0);
  strokeWeight(4);
  line (30, 400, 30, 450);

  stroke(0, 0, 0);
  strokeWeight(4);
  line (70, 400, 70, 450);

}

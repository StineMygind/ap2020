//Defining slider
let slider;

function setup() {
createCanvas(700,500)
  background(100,100,0);
  //Slider
  slider = createSlider(165, 280, 190);
  slider.position(310, 420);
  slider.style('width', '80px');
}

function draw() {

  background(144, 146, 141);
  fill(192, 93, 249);
  rect(270, 405, 160, 50, 10);


//Woman
  //Hair
      fill(247,221,27);
    rect(90, 60, 120, 140,40);
  //head
      fill(248,201,144);
    ellipse(150,120,100);
  //Face
  // Eyes - white
      fill(255);
    ellipse(135,110,20);
    ellipse(165,110,20);
  // Eyes - Black
      fill(0);
    ellipse(135,110,10);
    ellipse(165,110,10);
  // Eyelashes
      noFill();
    arc(127, 98, 8, 8, HALF_PI, PI);
    arc(125.5, 100, 8, 8, HALF_PI, PI);
    arc(124.5, 102.5, 8, 8, HALF_PI, PI);
    arc(173, 98, 8, 8, 0, HALF_PI);
    arc(174.5, 100, 8, 8, 0, HALF_PI);
    arc(175.5, 102.5, 8, 8, 0, HALF_PI);
  //Mouth
      noFill();
    arc(150, 135, 50, 30, 2*PI, PI);
  //Arms
      fill (0);
    line(80,200,150,250)
    line(220,200,150,250)
  //Dress
      fill ('red');
    triangle(150, 170,slider.value(), 350, 295-slider.value(), 350);
  //Legs
      fill (0);
    rect(130, 350, 15, 40);
    rect(150, 350, 15, 40);
  //Feet
      fill(255, 174, 0)
    ellipse(130, 390, 30, 10);
    ellipse(165, 390, 30, 10);

//Man
  //Hair
      fill (140, 94, 35);
    arc(525, 100, 95, 100, PI, 0, CHORD);
  //Head
      fill (248,201,144);
    ellipse(525,120,100);
  //Face
  //Eyes - white
      fill(255);
    ellipse(510,110,20);
    ellipse(540,110,20);
  //Eyes - black
      fill(0);
    ellipse(510,110,10);
    ellipse(540,110,10);
  //Mouth
      noFill();
    arc(525, 135, 50, 30, 2*PI, PI);
  //Arms
      fill (0);
    line(430,230,525,170)
    line(620,230,530,170)

  //Shirt
      fill (70, 127, 214);
    rect(550-0.3*slider.value(), 170, -65+0.7*slider.value(), 150,7);
  //Legs
      fill (10,10,10);
    rect(510, 320, 15, 70);
    rect(530, 320, 15, 70);

  //Feet
      fill(30, 178, 43)
    ellipse(510, 390, 30, 10);
    ellipse(545, 390, 30, 10);



}

MiniEx2

![ScreenShot](screenshot.png)

https://stinemygind.gitlab.io/ap2020/MiniEx2

For miniEx 2 I have made two emojis which looks like a man and a woman. I have used different shapes like rectangle, ellipse, triangle and also an arc which I found more difficult to code the way I wanted it to be. I have been colouring the different shapes, so they looked more like humans. To find the colours I have been using this site: https://htmlcolorcodes.com, which I found much easier than just trying different possibilities in Atom. I have created a slider which is coupled to the size of the woman’s dress and the man’s shirt, so the user can choose by itself how big the emoji should be. This took me some time because it was difficult to figure out how to couple the size and slider with each other. It was easy to create the slider because it can be found in the references, but after putting it in my code I had to find out how the slider worked, to use the value of the slider for the size of the triangle and rectangle. I think it can be done in different ways, but I put “slider-value” in the coordinates for my rectangle and triangle, and then I just tried different numbers to make it fit. The triangle and rectangle were different from each other because the syntax is created differently. To make a triangle you must write three different coordinates but to make a rectangle you must write one coordinate and then the width and height, so that is why I use the minus and the plus for the dress, and multiplication for the rectangle.

Before I started out making these emojis I reflected on the text that we have read. It raised multiple questions, like how many emojis should be made to cover all humans preferences. The problem of making the emojis more personal is that some people always will be left out. After looking at my iPhone emoji keyboard, I realised that the human emojis are all thin. Recently I heard that there are now more people who are overweight than underweight, so it surprises me that none of the emojis has an overweight appearance. I wanted to make two emojis where the user itself could regulate how over- or underweight the emojis should be, and the outcome is what you see. But it lefts my emojis with other problems like gender, skin tone and the clothes colouring. I think that it will be difficult to cover all human characteristics and make everybody feel seen, so I’m actually not sure, that it would be a good idea trying to do that. Like in the text, trying to make up for things is actually creating more issues, so it is kind of a never-ending project.
But here I have tried to cover the weight issue, but I could consider coding it to be more flexible and recognisable for everyone. Right now I’m not that good at coding, which also makes it more difficult for me to cover all the different aspects of the emojis.

References:

https://p5js.org/examples/dom-slider.html

https://www.youtube.com/watch?v=587qclhguQg


**MiniEx 10**

My own flowchart: https://8jrkgr.axshare.com

My own flowchart is based on my MiniEx6 (https://gitlab.com/StineMygind/ap2020/-/tree/master/public/MiniEx6). 
It represents a game and I found it very difficult to organize the different elements from the game and at the same time try to show the conceptual idea of the game. I choose to divide the flowchart in the start into the four elements from my game and below them, I described how they function. This gives the result that the end of the flowchart isn’t at the end of everything, and I’m a bit confused if this is “allowed”. I could have chosen to make the flowchart follow the experience of playing the game, but I found that much more complex because of several opportunities that then would appear, and that would be less understandable/simple, which was one of the criteria for this flowchart.

I liked making the flowchart because it made me reflect a lot upon how I should create it to make it as simple as possible. I think that some of the challenges were to comprehend and merge the algorithmic procedures so it could be turned into simple and few boxes in the flowchart. But I also liked to do that, because I think it would be a lot more complex to do the opposite and describe the algorithmic procedure with all its details. 

I think the value of making a flowchart that most people can understand, is that it makes communication much easier due to the flowchart as some kind of common starting point. As the text “The Multiple Meanings of a Flowchart” by Nathan Ensmenger, also points out, the flowchart is characterized as a boundary object, which is “*an artefact that simultaneously inhabits multiple intersecting social and technical worlds. In each of these worlds, the boundary object has a well- defined meaning that “satisf[ies] the informational requirements*” of the members of that community; at the intersection of these worlds, the boundary object is flexible enough in meaning to allow for conversation between multiple communities” (p. 324).

I think there is a huge difference between making a flowchart individually and in collaboration because when making it together with someone else you bring more reflections, possibilities, arguments and so on to the discussion and making of the flowchart, which I think is very valuable. 


**Written together with study group - reflections upon our flowcharts**

https://zwz5sk.axshare.com 

Our first flowchart is made upon an idea concerning the theme data capture. The idea is that you open the program and see four questions (with buttons named “contribute”), a bar that shows what level you are on and a next button. The questions appearing are easy to answer, but when continuing they get more personal. The level bar’s increment depends on how many questions the user has answered and the questions themselves as they have a value depending on the level of being personal.
There is still space to further develop the idea, as we want to program that “ads”, about friends answering the questions, pop up to engage the user to answer more questions.

https://gip2go.axshare.com 

Our second flowchart represents a program which illustrates two understandings/perceptions of data capture. At first, the user sees the positive side of data capture, which is illustrated by pieces of texts and interactive elements. The user can “flip” or change the canvas, and the user now sees the negative side of data capture. We intend to show that users of the internet are automatically dragged into the capitalist illusion of data capture positivity, but have to actively search for the negative effects of data capture which are normally hidden.

We have made our flowchart so it makes sense to ourselves. We used flowcharting as a way of communicating conceptual and technical aspects of our program to reach a common understanding. 
Right now the flowchart is a sketch and it only consists of the basic concept, and it could be improved and specified in many different ways. It is therefore not a blueprint, but a starting point from where our communication and programming can take place and develop. Hopefully, the flowchart can also give us a place where we can always return to, in order to grasp our conceptual thoughts and see how they have been connected to our technical thoughts. Our concept and ideas were developed before diving into the flowcharting and therefore it reflects our conceptual thoughts and what we value as the most important elements. 

The approach we have had in making our two ideas as flowchart is that it should be a help for us as a group to develop our ideas and to concretize how we wanted the program to function. It helped us to find coherence between our individual ideas and conception of the program. The intention behind our flowcharts is to get closer to thoughts about how the program should function technically. This is why we still have some programming terms implemented, as we saw it relevant later in the process of making our final project. 

There are several technical challenges in our two programs. We assume that it will be very difficult to make the eyes which follow either the text the user writes (idea with questions/ranking) or the mouse (idea with the backside of the con). Furthermore, we have many elements in both ideas which are time consuming, difficult to make and need to function together. We have a logical understanding of how the programs should work technically, but we fear that there might be something we have overlooked. Because we aren’t sure of which problems we will meet yet, we think will address them through trial and error and use the reference in p5.js, Daniel Shiffman and other things on the internet to solve the challenges as they occur.

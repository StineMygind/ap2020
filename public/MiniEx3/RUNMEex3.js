function setup() {
createCanvas(windowWidth, windowHeight);

}
function draw() {
background(250, 41, 79);
  drawElements();

	push();

  translate(width/2, height/2);
  	for (var i = 0; i < 13; i++) {

  push();

  rotate(TWO_PI * i / 12);
  strokeWeight(3)
  line(70, 0, 90, 0);

  pop();
}
	pop();

  push();

  translate(windowWidth/2, windowHeight/2);
    rotate(radians(frameCount*0.5));
    strokeWeight(2);
    fill(0);
    line(0,0,40,40);

  pop();
  push();

  translate(windowWidth/2, windowHeight/2);
    rotate(radians(frameCount*6));
    strokeWeight(2);
    fill(0);
    line(0,0,70,70);

  pop();

}

function drawElements() {

fill(255);
ellipse(windowWidth/2, windowHeight/2, 200, 200);

}

function windowResized() {
  resizeCanvas(windowWidth, windowHeight);
}

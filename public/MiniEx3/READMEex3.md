MiniEx3

![ScreenShot](screenshot.png)

https://stinemygind.gitlab.io/ap2020/MiniEx3

For this miniEx, I have created a clock as my throbber. The clock has two hands and is placed in the middle of the window which has a red background. In class and in the readings we have explored how throbbers frustrate people because the time, when a throbber is present, reminds people of waiting and wasted time. With my throbber, I wanted to express how we feel about throbbers and make it clear that time is being wasted when a throbber is shown. So I have tried to create some kind of provocation.
If looking at my syntaxes you can see that the canvas is resizing itself depending on how big the users' window is and how it gets widened. The canvas adjusts the users' adjustments as well as the clock. It is placed in the centre of the window, which can be seen in this syntax: 

fill(255);

ellipse(windowWidth/2, windowHeight/2, 200, 200);

In computation time doesn’t really exist, because it is more like a large number of processes inside the computer working on doing what it is being told and following the “rules” that it has been made by and made out of. But it is possible to create time by giving the computer the right instructions. For my throbber, I decided to make clear the time that was being wasted which is illustrated by how fast the hands are moving. So my time-related syntax is the rotation because the rotation is depending on the frame count and the number I have chosen to multiply it with. The higher the number, the faster the clock moves.

rotate(radians(frameCount*0.5));

rotate(radians(frameCount*6));

Reflections about a specific throbber
Once the throbber on Youtube was a spinning icon which was able to turn into a snake game, where the user was the snake and could play in that amount of time where a video was buffering. The throbber consisted of ellipses forming a circle which turned into the body of the snake when the arrow keys were pressed. This is a good way to keep the user busy instead of putting the user in a waiting position. Youtube tries to hide the time that is being wasted and hide what is happening behind the screen. In some way, the throbber suddenly has more positive connotations which break away from the usual perception of throbbers.

References

https://genekogan.com/code/p5js-transformations/

https://htmlcolorcodes.com




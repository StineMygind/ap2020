let points = 40;
let value1
let level = 1;
let button;

let keyValue = 0;
let mouseValue = 0;
let clickValue = 0;

let coins = []; // array for the number of coins visible on the screen
let coinsNumber = 1; //variabel der bruges på ting der tjenes penge ved





function preload() { //Preload to load the json-files before loading the page
  dataQuestions = loadJSON('questions.json');

}
function setup() {
  createCanvas(1350, 750);
  background(220);
  questions();
  topUsers();
  // Create an Audio input
mic = new p5.AudioIn();
// start the Audio Input.
// By default, it does not .connect() (to the computer speakers)
mic.start();

coins = new Coin();
  // create new coins
  for (let i = 0; i < coins.length; i++){ // used to create new coins
    coins[i] = new Coin();
  }

}

function draw() {
  levelbar();
  businessSide();
  logoEyes();
  buttonNext();



  for (let i = 0; i < coins.length; i++) { //objekter bliver ved med at tegnes
  coins[i].move();
  coins[i].show();

}


}
class Coin {
  constructor() { //svarer til objektets setup
    this.speed = random(1,2);
    this.size = (25,25);
    this.pos = new createVector(random(1015,1395),105);
  }
  move() {
  this.pos.y = this.pos.y + this.speed;
}
  show() {
    push();
    stroke('#FFDB33');
    strokeWeight(3);
    fill('#FFF333');
    ellipse(this.pos.x, this.pos.y, this.size, this.size);
    textSize(14);
    textAlign(CENTER);
    fill('#FFDB33');
    stroke(0); //evt orange
    strokeWeight(1);
    text("$", this.pos.x, this.pos.y+4.5);
    pop();
  }
}

function logoEyes() {
noStroke();
fill(60, 90, 255);
rect(0,0,1400,100);
stroke(0);
fill(0);
textAlign(CENTER);
text("DataBook", width/2, 90);
textSize(100);

movingEyeX1 = map(mouseX, 0, width, 750, 770);
movingEyeX2 = map(mouseX, 0, width, 805, 825);
movingEyeY = map(mouseY, 0, height, 67, 80);

fill(0);
ellipse(movingEyeX1,movingEyeY,17,17);
ellipse(movingEyeX2,movingEyeY,17,17);
}

function questions() {
let yPos=100;
//jeg har slettet let under for-loopet, fordi det ikke var nødvendingt, da det bare er definitioner, tror jeg. ændrer sig måske med next-button
for (let i =0; i < 4; i++) {
  let randomDifficulty = random([0, 1, 2, 3, 4]);
  let questions = dataQuestions.allQuestions[randomDifficulty].questions;
  let value = dataQuestions.allQuestions[randomDifficulty].value;
  let randomQuestion = random(questions);
  createP(randomQuestion).position(100,yPos*i+200);
//inputfelter
  createInput("").position(100,50+yPos*i+200).size(400,15).attribute('placeholder', 'Earn x points here'.replace("x",value));
//antal point spørgsmålet giver. Er placeret bag contribute-button
  createP("+"+value).position(540,235+i*100);
//contribute buttons
  button = createButton("Contribute").position(530,250+i*100).size(75,20).style('background-color', "#05004E");
  button.mousePressed(button.hide);
  // button.mousePressed(gainPoints);

    if (value == 100) {
      points + 10
    } else if (value == 200) {
      points + 20
    } else if (value == 300) {
      points + 30
    } else if (value == 400) {
      points + 40
    } else if (value == 500) {
      points + 50
    }


//  button.mousePressed(gainPoints);

//check at det virker
  print(value);
  print(randomDifficulty);

 }
}


function topUsers() {
  let userX = 670
  let userY = 310

  push();
    textSize(22);
    fill("#05004E");
    textAlign(CENTER);
    text("TOP 5 USERS", 820, 270);
    strokeWeight(2)
    line(720, 280, 919, 280);
  pop();

  // Top1
    // Username
    textSize(15);
    fill("#05004E");
    textAlign(LEFT);
    text("Linda Johnson", userX, userY);

    // Level bar
    fill("#77D0FF");
    rect(userX+170, userY-12, 125, 13, 10);
    fill("#05004E");
    rect(userX+170, userY-8, 105, 5, 10);
    fill("#77D0FF");
    ellipse(userX + 170, userY-6, 38, 28);

    // Level number
    textSize(17);
    textAlign(CENTER);
    fill("#05004E");
    text("665", userX+170, userY);

  // Top2
  push();
    translate(0, 50);
      // Username
      textSize(15);
      fill("#05004E");
      textAlign(LEFT);
      text("Robert Smith", userX, userY);

      // Level bar
      fill("#77D0FF");
      rect(userX+170, userY-12, 125, 13, 10);
      fill("#05004E");
      rect(userX+170, userY-8, 30, 5, 10);
      fill("#77D0FF");
      ellipse(userX + 170, userY-6, 38, 28);

      // Level number
      textSize(17);
      textAlign(CENTER);
      fill("#05004E");
      text("597", userX+170, userY);
  pop();

  // Top3
  push();
    translate(0, 100);
      // Username
      textSize(15);
      fill("#05004E");
      textAlign(LEFT);
      text("Maria Rogers", userX, userY);

      // Level bar
      fill("#77D0FF");
      rect(userX+170, userY-12, 125, 13, 10);
      fill("#05004E");
      rect(userX+170, userY-8, 75, 5, 10);
      fill("#77D0FF");
      ellipse(userX + 170, userY-6, 38, 28);

      // Level number
      textSize(17);
      textAlign(CENTER);
      fill("#05004E");
      text("572", userX+170, userY);
  pop();

  // Top4
  push();
    translate(0, 150);
      // Username
      textSize(15);
      fill("#05004E");
      textAlign(LEFT);
      text("Jane Coleman Wright", userX, userY);

      // Level bar
      fill("#77D0FF");
      rect(userX+170, userY-12, 125, 13, 10);
      fill("#05004E");
      rect(userX+170, userY-8, 90, 5, 10);
      fill("#77D0FF");
      ellipse(userX+170, userY-6, 38, 28);

      // Level number
      textSize(17);
      textAlign(CENTER);
      fill("#05004E");
      text("565", userX+170, userY);
  pop();

  // Top5
  push();
    translate(0, 200);
      // Username
      textSize(15);
      fill("#05004E");
      textAlign(LEFT);
      text("Martin Cook Morris", userX, userY);

      // Level bar
      fill("#77D0FF");
      rect(userX+170, userY-12, 125, 13, 10);
      fill("#05004E");
      rect(userX+170, userY-8, 40, 5, 10);
      fill("#77D0FF");
      ellipse(userX+170, userY-6, 38, 28);

      // Level number
      textSize(17);
      textAlign(CENTER);
      fill("#05004E");
      text("501", userX+170, userY);
  pop();
}


function levelbar() {
  push();
  translate(-265, -70);
  fill("#77D0FF");
  rect(width/2-200, 185, 300, 25, 15);
  fill("#05004E");
  rect(width/2-200, 191.5, points, 12, 15);
  fill("#77D0FF");
  ellipse(width/2-200, 198, 70, 50);

  //level
  textSize(30);
  textAlign(CENTER);
  fill("#05004E");
  text(level, width/2-202, 209);
  pop();
}

function nextLevel() {
  if (points == 292) {
   points = 40
   level++
  }
}

function micValue() {
  let volValue = mic.getLevel()*1000;
  // print(vol);//vol er den værdi, som vi kan bruge i et if-statement i forhold til hvor hurtigt/hvor mange mønter der skal falde
  if (volValue>10) {
    print(volValue);
  }
}

// function xxxx(){
//   //keyValue = keyValue + 10;
//   if (keyIsPressed) {
//     coins.push(new Coin());
//
//   }
//
// }

function mouseMoved(){
  mouseValue = mouseValue + 0.5;
  print(mouseValue);
  return false;
}

function mouseClicked(){
  clickValue = clickValue + 10;
  print(clickValue);
  return false;
}

// function gainPoints() {
// rect(30,30,300,300);
//   if (value1 == 100) {
//     points + 10
//   } else if (value1 == 200) {
//     points + 20
//   } else if (value1 == 300) {
//     points + 30
//   } else if (value1 == 400) {
//     points + 40
//   }
// }


function businessSide() {
  rect(1000, 0, 350, 750);
  if (mouseX > 1000) {
        noCursor();
      } else {
        cursor('default');
      }
}

function buttonNext (){
nextButton = createButton('Next');
nextButton.style('background-color', 'blue');
nextButton.style("border-radius","12px");
nextButton.style('border', 'blue')
nextButton.style('padding', "10px 14px")
nextButton.style('font-size', '25px');
nextButton.position(320,600);
//nextButton.mousePressed(questions);

}

// function ButtonNextPushed() {
//  Stine -  questions.splice(variabel for det spørsmål der bliver valgt,1);
//  Olivia
// array[i].splice(value);
//
// for(i=0;i<array.lenght; i++);
// array[].push(value / i);
// array[].push(random())

// }

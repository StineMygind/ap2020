function preload() { //Preload to load the json-files before loading the page
  dataQuestions = loadJSON('questions.json');
}

function setup() {
  createCanvas(1350, 750);
  setupQuestions(); // Function to create and change the questions
  // Create an Audio input and start it
  mic = new p5.AudioIn();
  mic.start();
}

function draw() {
  background("#3d6098");

  // Calling companySide functions
  wall();
  companyCursor();
  skyline();
  micValue();
  makeAndStopCoins();

  // Calling pageSetup functions
  logoEyes();
  topUsers();
  levelbar();
  nextLevel();

  // Frame around sketch
  push();
    stroke(4);
    noFill();
    rect(0,0,1350,750);
  pop();

}

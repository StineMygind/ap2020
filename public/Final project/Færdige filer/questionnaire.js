function setupQuestions() {
  let yPos = 100;
  // Declaration of variables
  let randomDifficulty; // Initiated in line 40
  let jsonQuestions; // Initiated in line 42
  let value; // Initiated in line 44
  let randomQuestion; // Initiated in line 46

  // Empty arrays for setting up the questionnaire
  let questions = [];
  let inputFields = [];
  let points = [];
  let contributeButtons = [];

  // Creating the first loop of questions, inputfields, contributeButtons and values
  for (let i = 0; i < 4; i++) {
    drawQuestion(yPos, questions, inputFields, points, contributeButtons, i);
  }

  // Creating and styling next-button
  nextButton = createButton('NEXT')
    .size(85, 30)
    .style('background-color', "#B2DAFF")
    .style('color', "#05004E")
    .style('font-weight', "bold")
    .style('border-radius',"8px")
    .style('border', 'none')
    .style('padding', "2px 2px")
    .style('font-size', '15px')
    .style('text-align', 'center')
    .position(60, 640)
    // Calling function with parameters to bring local variables to function newQuestions
    // if next-button is pushed
    .mousePressed(function() {newQuestions(yPos, questions, inputFields, points, contributeButtons)});
}

// Creating questions, inputfields, contributeButtons and their value
function drawQuestion(yPos, questions, inputFields, points, contributeButtons, i) {
  // Picking random number to decide which array of difficulty to load questions from
  randomDifficulty = random([0, 1, 2, 3, 4]);
  // Loading questions within one difficulty from JSON-file
  jsonQuestions = dataQuestions.allQuestions[randomDifficulty].questions;
  // Loading values from JSON-file
  value = dataQuestions.allQuestions[randomDifficulty].value;
  // Picking random question within the chosen difficulty
  randomQuestion = random(jsonQuestions);

  // Creating paragraphs for each question and styling them
  questions[i] = createP(randomQuestion)
    .position(60, yPos * i + 210)
    .style('font-size', '13px')
    .style ('color',"#FFFFFF")
    .style('font-family', 'Verdana');

  // Creating and styling inputfields
  inputFields[i] = createInput("")
    .position(60, yPos * i + 210 + 40) // 210 is the distance between questions and 40 is the distance between questions and inputfields
    .size(440, 15)
    .value("")
    .attribute('placeholder', 'Answer and earn x points'.replace("x",value));

  // The number of points that each question give. Placed behind the contributeButton
  points[i] = createP("+" + value)
    .position(540, yPos * i + 239)
    .style('color', "#FFFFFF")
    .style('font-family', 'Verdana')
    .style('font-size', '13px');

  // Creating and styling contribute buttons
  contributeButtons[i] = createButton("CONTRIBUTE")
    .position(530, yPos * i + 250)
    .size(85, 24)
    .style('background-color', "#05004E")
    .value(value) // Assigning the value of the question from the JSON-file to the button. Makes currentPoints increase
    .style('color', "#FFFFFF")
    .style('border', 'none')
    .style('text-align', 'center')
    .style('padding', "5px 5px")
    .style('border-radius', "8px")
    .mousePressed(function(){ buttonPressed(contributeButtons[i], inputFields[i], i)});
}

function removeQuestion(yPos, questions, inputFields, points, contributeButtons, i) {
  // Removing elements
  questions[i].remove();
  inputFields[i].remove();
  points[i].remove();
  contributeButtons[i].remove();
}

function newQuestions(yPos, questions, inputFields, points, contributeButtons) {
  // Removing elements to enable creating new ones
  for (let i =0; i < 4; i++) {
    removeQuestion(yPos, questions, inputFields, points, contributeButtons, i);
  }
  // Creating new elements
  for (let i =0; i < 4; i++) {
    drawQuestion(yPos, questions, inputFields, points, contributeButtons, i);
  }
}

function buttonPressed(contributeButton, inputField, i) { // Parameters bringing in arguments from contributeButton.mousePressed (Line 80)
  // Creating variable which equals the answer in inputfield
  let input = inputField.value();
  // Creating an alert if contribute-button is pushed and inputfield isn't filled
  if (input == "") {
    alert("Please type an answer");
    return false;
  }
  else {
    // Hiding contribute-button if pushed and inputfield filled
    contributeButton.hide();
    // Disables inputfields
    inputField.attribute("readonly", true);
    // Adding the value of the contributeButton/question to currentPoints
    currentPoints = (contributeButton.value())/10 + currentPoints;
    return true;
  }
}

let currentPoints = 40; // Points that increase when contributing
let level = 1; // Variable to define level of user

function logoEyes() {
  // Styling of logo
  fill("#FFFFFF");
  rect(0, 0, 1400, 80);
  stroke(0);
  fill("#213159");
  textFont('Verdana');
  textAlign(CENTER);
  text("QUESTION ROOM", width/2, 62);
  textSize(50);

  // Mapping the coordinates of the pupils within the two o's to the coordinates of the mouse-movements on the canvas
  movingEyeX1 = map(mouseX, 0, width, 791, 800); // Horisontal movements of left eye
  movingEyeX2 = map(mouseX, 0, width, 831, 840); // Horisontal movements of right eye
  movingEyeY = map(mouseY, 0, height, 45, 52); //  Vertical movements of both eyes

  push();
    noStroke();
    fill("#213159");
    ellipse(movingEyeX1, movingEyeY, 15, 15); // The pupil in the left eye
    ellipse(movingEyeX2, movingEyeY, 15, 15); // The pupil in the right eye
  pop();
}

function levelbar() {
// Styling the levelbar
  push();
    translate(-125, -50);
    fill("#B2DAFF");
    rect(width/2 - 200, 185, 300, 25, 15);
    fill("#05004E");
    rect(width/2 - 200, 191.5, currentPoints, 12, 15); // currentPoints defines the length the rectangle that illustrates the sum of the points
    fill("#B2DAFF");
    ellipse(width/2 - 200, 198, 70, 50);

    // Level number
    textSize(30);
    textAlign(CENTER);
    fill("#05004E");
    text(level, width/2-202, 209); // The variable level depends on function nextLevel
  pop();
}

function nextLevel() {
  // Changing the level if the points reach the end of the levelbar
  if (currentPoints >= 290) { // 290 is the maximum length of the rectangle
    currentPoints = 40; // The starting value that the rectangle returns to, when reaching next level
    level++; // Incrementation of level
  }
}

function topUsers() {
  let userX = 710;
  let userY = 285;

  push();
    textSize(22);
    fill("#05004E");
    textAlign(CENTER);
    text("TOP USERS", 820, 235);
    strokeWeight(2)
    line(720, 248, 919, 248);
  pop();

  // Top1
  // Username
  push();
    textSize(10);
    fill("#05004E");
    textAlign(LEFT);
    text("Linda Johnson", userX, userY);

    // Level bar
    fill("#B2DAFF");
    rect(userX+130, userY-8, 90, 7, 10);
    fill("#05004E");
    rect(userX+130, userY-5.5, 80, 2, 10);
    fill("#B2DAFF");
    ellipse(userX + 130, userY-4, 26, 17);

    // Level number
    textSize(10);
    textAlign(CENTER);
    fill("#05004E");
    text("665", userX+130, userY);
  pop();

  // Top2
  push();
    translate(0, 40);
    // Username
    textSize(10);
    fill("#05004E");
    textAlign(LEFT);
    text("Robert Smith", userX, userY);

    // Level bar
    fill("#B2DAFF");
    rect(userX+130, userY-8, 90, 7, 10);
    fill("#05004E");
    rect(userX+130, userY-5.5, 20, 2, 10);
    fill("#B2DAFF");
    ellipse(userX + 130, userY-4, 26, 17);

    // Level number
    textSize(10);
    textAlign(CENTER);
    fill("#05004E");
    text("597", userX+130, userY);
  pop();

  // Top3
  push();
    translate(0, 80);
    // Username
    textSize(10);
    fill("#05004E");
    textAlign(LEFT);
    text("Maria Rogers", userX, userY);

    // Level bar
    fill("#B2DAFF");
    rect(userX+130, userY-8, 90, 7, 10);
    fill("#05004E");
    rect(userX+130, userY-5.5, 60, 2, 10);
    fill("#B2DAFF");
    ellipse(userX + 130, userY-4, 26, 17);

    // Level number
    textSize(10);
    textAlign(CENTER);
    fill("#05004E");
    text("572", userX+130, userY);
  pop();

  // Top4
  push();
    translate(0, 120);
    // Username
    textSize(10);
    fill("#05004E");
    textAlign(LEFT);
    text("Jane Coleman Wright", userX, userY);

    // Level bar
    fill("#B2DAFF");
    rect(userX+130, userY-8, 90, 7, 10);
    fill("#05004E");
    rect(userX+130, userY-5.5, 50, 2, 10);
    fill("#B2DAFF");
    ellipse(userX + 130, userY-4, 26, 17);

    // Level number
    textSize(10);
    textAlign(CENTER);
    fill("#05004E");
    text("565", userX+130, userY);
  pop();

  // Top5
  push();
    translate(0, 160);
    // Username
    textSize(10);
    fill("#05004E");
    textAlign(LEFT);
    text("Martin Cook Morris", userX, userY);

    // Level bar
    fill("#B2DAFF");
    rect(userX+130, userY-8, 90, 7, 10);
    fill("#05004E");
    rect(userX+130, userY-5.5, 85, 2, 10);
    fill("#B2DAFF");
    ellipse(userX + 130, userY-4, 26, 17);

    // Level number
    textSize(10);
    textAlign(CENTER);
    fill("#05004E");
    text("501", userX+130, userY);
  pop();

  // Top6
  push();
    translate(0, 200);
    // Username
    textSize(10);
    fill("#05004E");
    textAlign(LEFT);
    text("Sally Reed Gray", userX, userY);

    // Level bar
    fill("#B2DAFF");
    rect(userX+130, userY-8, 90, 7, 10);
    fill("#05004E");
    rect(userX+130, userY-5.5, 65, 2, 10);
    fill("#B2DAFF");
    ellipse(userX + 130, userY-4, 26, 17);

    // Level number
    textSize(10);
    textAlign(CENTER);
    fill("#05004E");
    text("484", userX+130, userY);
  pop();

  // Top7
  push();
    translate(0, 240);
    // Username
    textSize(10);
    fill("#05004E");
    textAlign(LEFT);
    text("Lisa Nelson", userX, userY);

    // Level bar
    fill("#B2DAFF");
    rect(userX+130, userY-8, 90, 7, 10);
    fill("#05004E");
    rect(userX+130, userY-5.5, 30, 2, 10);
    fill("#B2DAFF");
    ellipse(userX + 130, userY-4, 26, 17);

    // Level number
    textSize(10);
    textAlign(CENTER);
    fill("#05004E");
    text("483", userX+130, userY);
  pop();

  // Top8
  push();
    translate(0, 280);
    // Username
    textSize(10);
    fill("#05004E");
    textAlign(LEFT);
    text("Jack James Collins", userX, userY);

    // Level bar
    fill("#B2DAFF");
    rect(userX+130, userY-8, 90, 7, 10);
    fill("#05004E");
    rect(userX+130, userY-5.5, 40, 2, 10);
    fill("#B2DAFF");
    ellipse(userX + 130, userY-4, 26, 17);

    // Level number
    textSize(10);
    textAlign(CENTER);
    fill("#05004E");
    text("439", userX+130, userY);
  pop();
}

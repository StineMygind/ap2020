// The initial values of interactions
let keyValue = 0;
let mouseValue = 0;
let clickValue = 0;
let coins = []; // array for the number of coins visible on the screen

// Class of the coin objects
class Coin {
  constructor() {
    this.speed = random(1, 2);
    this.size = (25, 25);
    this.pos = new createVector(random(1015, 1395), 75);
  }

  // Make the coins fall and stop when they hit the bottom of the canvas
  move() {
    this.pos.y = this.pos.y + this.speed;
    // Making the coins stop if they hit the bottom of the canvas
    if (this.pos.y >= height - this.size/2){
      this.speed = 0;
    }
  }

  // Design of the coins
  show() {
    push();
      stroke('#FFDB33');
      strokeWeight(3);
      fill('#FFF333');
      ellipse(this.pos.x, this.pos.y, this.size, this.size);
      textSize(14);
      textAlign(CENTER); // Start coordinate to center the dollar sign
      fill('#FFDB33');
      stroke(0);
      strokeWeight(1);
      text("$", this.pos.x, this.pos.y + 4.5); // Center $
    pop();
  }

  // Function to check the distance between the coins.
  coinsTouching(other){
    let d = dist(this.pos.x, this.pos.y, other.pos.x, other.pos.y);
    if (d < this.size/3 && other.speed == 0) {
      // Return true enables the coinsTouching in the function makeAndStopCoins
      return true;
    } else {
      // Return false is used to prevent default mistakes.
      return false;
    }
  }
}

function makeAndStopCoins(){
  // Create the coins
  for (let i = 0; i < coins.length; i++) {
    coins[i].move();
    coins[i].show();
    // If it is two different coins and coinsTouching = true, the coin stops
    for (var j = 0; j < coins.length; j++) {
      if (coins[i] !== coins[j] && coins[i].coinsTouching(coins[j])) {
        coins[i].speed = 0;
      }
    }
  }
}

function micValue() {
  // getLevel is a p5.js function which measures the sound volume and maps it between a value of 0 and 1
  let volValue = mic.getLevel();
  // Maps this between a value of 0 and 15
  let mappedVol = floor(map(volValue, 0, 1, 0, 15));
  if (mappedVol > 2.5) {
    coins.push(new Coin);
  }
}

function keyPressed() {
  // keyPressed is an event function. Every time a key is pressed keyValue increases by one and a coin is pushed in.
  keyValue++;
  if(keyValue > 1) {
    coins.push(new Coin);
    // KeyValue = 0 so coins won't be pushed
    keyValue = 0;
  }
  // Prevents defaults mistakes
  return false;
}

function mouseMoved() {
  // mouseMoved is an event function. Every time the mouse is moved one pixel mouseValue increases by one.
  mouseValue++;
  if(mouseValue > 10) {
    coins.push(new Coin);
    // mouseValue = 0 so coins won't be pushed
    mouseValue = 0;
  }
  // Prevents defaults mistakes
  return false;
}

function mouseClicked() {
  coins.push(new Coin);
  return false; // To prevent default
}

function companyCursor() {
  fill("#213159");
  rect(1000, 0, 350, 750);
  // When the mouse is on the company side, there is no mouse to underline that the user cannot get any of the money he or she generates
  if (mouseX > 1000) {
    noCursor();
  } else {
    cursor('default');
  }
}

function wall() {
  // The dividing of the two sides
  push();
    fill(200);
    rect(990, 80, 10, 680);
  pop();
}

function skyline() {
  let skylineX = 1000;
  let skylineY = 750;

  // The silhouette skyline on the company side
  push();
    noStroke();
    fill("#1B274D");
    rect(skylineX, skylineY, 400, - 90);
    rect(skylineX, skylineY, 40, - 200);
    rect(skylineX + 45, skylineY, 40, - 160);
    rect(skylineX + 90, skylineY, 40, - 300);
    rect(skylineX + 140, skylineY, 80, - 250);
    rect(skylineX + 230, skylineY, 60, - 200);
    rect(skylineX + 230, skylineY, 60, - 200);
    rect(skylineX + 170, skylineY, 20, - 300);
    rect(skylineX + 200, skylineY, 40, - 130);
    rect(skylineX + 295, skylineY, 65, - 150);
    rect(skylineX + 260, skylineY, 5, - 300);
    rect(skylineX + 15, skylineY, 5, - 250);
    rect(skylineX + 50, skylineY, 30, - 180);
    rect(skylineX + 70, skylineY, 30, - 130);
    ellipse(skylineX + 330, skylineY - 160, 60);
    ellipse(skylineX + 110, skylineY - 300, 50);
  pop();
}

let points = 150;
let level = 1;


function preload() { //Preload to load the json-files before loading the page
  dataQuestions = loadJSON('questions.json');

}
function setup() {
  createCanvas(1350, 750);
  background(220);
  questions();
  //inputFields();
  //buttons();
}

function draw() {
  levelbar();
  businessSide();
  logoEyes();
  buttonNext();

}

function logoEyes() {
noStroke();
fill(60, 90, 255);
rect(0,0,1400,100);
stroke(0);
fill(0);
textAlign(CENTER);
text("DataBook", width/2, 90);
textSize(100);

movingEyeX1 = map(mouseX, 0, width, 750, 770);
movingEyeX2 = map(mouseX, 0, width, 805, 825);
movingEyeY = map(mouseY, 0, height, 50, 80);

fill(0);
ellipse(movingEyeX1,movingEyeY,17,17);
ellipse(movingEyeX2,movingEyeY,17,17);
}

function questions() {
let yPos=100;
//jeg har slettet let under for-loopet, fordi det ikke var nødvendingt, da det bare er definitioner, tror jeg. ændrer sig måske med next-button
for (let i =0; i < 4; i++) {
  randomValue = random([0, 1, 2, 3, 4]);
  questions = dataQuestions.allQuestions[randomValue].questions;
  value = dataQuestions.allQuestions[randomValue].value;
  randomQuestion = random(questions);
  createP(randomQuestion).position(150,yPos*i+200);
//inputfelter
  createInput("").position(150,50+yPos*i+200).size(400,15).attribute('placeholder', 'Earn x points here'.replace("x",value));
//antal point spørgsmålet giver. Er placeret bag contribute-button
  createP("+"+value).position(610,235+i*100);
//contribute buttons
  button = createButton("Contribute").position(600,250+i*100).size(75,20).style('background-color', "#05004E");
  button.mousePressed(button.hide);
//check at det virker
  print(value);
  print(randomValue);

  //____________________________________________

// for (let i =0; i < 4; i++) {
// let randomValue= random([1, 2, 3, 4, 5]);
// let questions=dataQuestions.allQuestions[i].questions;
// let value=dataQuestions.allQuestions[i].value;
//   createP(questions).position(150,yPos*i+200);
// //randomQuestion=(dataQuestions.allQuestions[randomValue].questions);
//
// print(value);
// //print(questions);
// print(randomValue);

  //____________________________________________

  //let x = dataQuestions.allQuestions.indexOf(value);
  //print (x);
//   print (dataQuestions.allQuestions[i].value);
//
//   for(let j =0; j < 4; j++) {
//     let value = dataQuestions.allQuestions[i].value;
//     createP(value).position(100,yPos*i+300);
//
 }
}
// }

//function inputFields() {
// let inputX=150
// let inputY=250
//læs denne for replace. value skal defineres og byttes ud
// input1=createInput("").position(inputX,inputY).size(400,15).attribute('placeholder', 'Earn x points here'.replace("x",value));
// input2=createInput("").position(inputX,inputY+100).size(400,15).attribute('placeholder', 'Earn x points here'.replace("x", value));
// input3=createInput("").position(inputX,inputY+200).size(400,15).attribute('placeholder', 'Earn x points here'.replace("x", value));
// input4=createInput("").position(inputX,inputY+300).size(400,15).attribute('placeholder', 'Earn x points here'.replace("x", value));

//}
// function hide(){
//  button.hide();
//  }
// function buttons() {
// let buttonX=600
// let buttonY=250
// button1 = createButton("Contribute").position(buttonX, buttonY).size(75,20).style('background-color', "#05004E"); //Button to make a new recipe, if it is pushed
// button2 = createButton("Contribute").position(buttonX, buttonY+100).size(75,20);
// button3 = createButton("Contribute").position(buttonX, buttonY+200).size(75,20);
// button4 = createButton("Contribute").position(buttonX, buttonY+300).size(75,20);
// }

function levelbar() {
  push();
  translate(-265, -70);
  fill("#77D0FF");
  rect(width/2-200, 185, 300, 25, 15);
  fill("#05004E");
  rect(width/2-200, 191.5, points, 12);
  fill("#77D0FF");
  ellipse(width/2-200, 198, 55, 55);

  //level
  textSize(30);
  textAlign(CENTER);
  fill("#05004E");
  text(level, width/2-202, 209);
  pop();
}

function businessSide() {
  rect(1000, 0, 350, 750);
  // if (mouseX > 1000) {
  //   cursor.hide();
  // } else {
  //   cursor.show();
  // }

}

function buttonNext (){
nextButton = createButton('Next');
nextButton.style('background-color', 'blue');
nextButton.style("border-radius","12px");
nextButton.style('border', 'blue')
nextButton.style('padding', "10px 14px")
nextButton.style('font-size', '25px');
nextButton.position(320,600);
//nextButton.mousePressed(questions);

}

// function ButtonNextPushed() {
//  Stine -  questions.splice(variabel for det spørsmål der bliver valgt,1);
//  Olivia
// array[i].splice(value);
//
// for(i=0;i<array.lenght; i++);
// array[].push(value / i);
// array[].push(random())

// }








//____________________________________________
//referring to the array "instructions" within the instruction json-file
// var questions2 = dataQuestions.allQuestions.questions2;
// var questions3 = dataQuestions.allQuestions.questions3;
// var questions4 = dataQuestions.allQuestions.questions4;
// randomQuestion1 = random(questions1);
// randomQuestion2 = random(questions2);
// randomQuestion3 = random(questions3);
// randomQuestion4 = random(questions4);
//For-loop to show 5 instructions each time the page is loaded or button is pushed
//question1();
// question2();
// question3();
// question4();


//
// button2 = createButton("Contribute"); //Button to make a new recipe, if it is pushed
// button2.position(330, 155);
// button2.mousePressed(button2pressed); //Refering to function reload
//
// button3 = createButton("Contribute"); //Button to make a new recipe, if it is pushed
// button3.position(330, 190);
// button3.mousePressed(button3pressed); //Refering to function reload
//
// button4 = createButton("Contribute"); //Button to make a new recipe, if it is pushed
// button4.position(330, 225);
// button4.mousePressed(button4pressed); //Refering to function reload
//

//




// function question1() {
//
//     createP(randomQuestion1);
// }

// function button1pressed() {
// //if (input1.value.length === 0) {
//   var value1 = dataQuestions.instructions.value1;
//   createP(value1).position(350, 105);
//   button1.hide();
//}
//remove button1
//splice??


// function question2() {
//     createP(randomQuestion2);
//
// }

// function button2pressed() {
//   var value2 = dataQuestions.instructions.value2;
//   createP(value2).position(350, 140);;
//   button2.hide();
// }

// function question3() {
//     createP(randomQuestion3);
// }

// function button3pressed() {
//   var value3 = dataQuestions.instructions.value3;
//   createP(value3).position(350, 175);;
//   button3.hide();
// }

// function question4() {
//     createP(randomQuestion4);
// }
// function button4pressed() {
//   var value4 = dataQuestions.instructions.value4;
//   createP(value4).position(350, 205);;
//   button4.hide();
// }

MiniEx8

![ScreenShot](screenshot.png)

Runme: https://stinemygind.gitlab.io/ap2020/MiniEx8

Title | Random recipe - By Olivia & Stine

Our work is called random recipe because it is random recipe instructions which contains random ingredients. Each time the button “click for new recipe” is pushed, a new random recipe will occur on the screen. We have used two json-files, one for the instructions and one for the ingredients. We chose to make random recipes because recipes resemble code in a way since both contain instructions of how to execute a task and both are also performative. Furthermore, we wanted to make both the instructions and the ingredients random, because in this way the recipes may be quite weird but they are still executable.

The json-files are used in the sketch in the function preload, where all the data from the files are loaded. The json-file with our instructions is downloaded from Github, and it consists of a list of instructions, which in the sentences contains {0} and {1} instead of ingredients. We have used our json-file with ingredients, which we have made by our selves,  to fill in {0} and {1}, by using the “replace” function. 
The button in the right corner is used to generate a new recipe each time it is pushed. We have used the syntax location.reload(); in a function called “reload”, which is used in button.mousePressed().

We like this quotation because it sums up the aim of the text and we think that is it relatable to our work; “This chapter aims to extend this discussion about the relation between spoken language and program code, but in particular to speculate on vocable code and the ways in which the body is implicated in coding practices. Through the connection to speech, programs can be called into action as utterances that have wider political resonance; they express themselves as if possessing a voice.”
It relates to our work by addressing how programs can express themself, and in our work we see code express itself through instructions in the code and also instructions in the program. We would argue that recipes are a combination of spoken language and code, because it uses the instructions from coding but it is written in our spoken language. We don’t see our program as something political, but our work illustrates how it easily could be turned into something political, if we had a different take on it - for instance by using political statements instead of recipe instructions. Furthermore, it illustrates a coherence between spoken language and code. 

We think that this quotation by Edsger Dijkstra is relevant when talking about json-files: “whereas machines must be able to execute programs (without understanding them), people must be able to understand them (without executing them).” We understand the json-files because we know the content, but the computer doesn’t know what it is and still it is able to execute the files. This expresses the relationship between humans and machines and how they are different from each other, like Dijkstra is talking about. The computer has both a readable and writable state level of language. The computer reads the code written in the program and then it writes by executing the program. When it is executing it doesn’t have to understand the content in the same way as humans, but the computer has to understand what has to be executed. Humans on the other hand understand the content but without it being a performative act.  
In our work we have implemented recipes, which encourage users to execute the program and thereby it becomes a performative act. By using recipes we have tried to give the users the same role as the computer by performing illocutionary acts. 

Github - https://github.com/dariusk/corpora/blob/master/data/foods/combine.json 




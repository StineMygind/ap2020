
function preload() { //Preload to load the json-files before loading the page
  dataInstructions = loadJSON('instructions.json'); //Recipe instuctions
  dataIngredients = loadJSON('ingredients.json'); //Ingrediens
}
function setup() {
  createCanvas(400, 100);
  textSize(32);
  fill(0);
  text("Recipe", 0,80);
      var instructions = dataInstructions.instructions; //referring to the array "instructions" within the instruction json-file
      var ingredients = dataIngredients.ingredients; //referring to the array "ingredients" within the instruction json-file
    for(let i =0; i < 5; i++) { //For-loop to show 5 instructions each time the page is loaded or button is pushed
      randomInstruction = random(instructions);
      createP(randomInstruction.replace('{0}', random(ingredients)).replace('{1}', random(ingredients)));

      button = createButton("New recipe"); //Button to make a new recipe, if it is pushed
      button.position(350, 40);
      button.mousePressed(reload); //Refering to function reload
    }
}

function reload() { //Function used for the button, to make the program reload, if it is pushed
  location.reload();
}

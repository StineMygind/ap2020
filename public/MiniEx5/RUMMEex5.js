function setup() {
  createCanvas(640, 480);
  background (0, 100, 300);
}
function draw() {
  background (0, 100, 300);

//flowers
  push();
//flower 1
if (mouseX<438 && mouseX>388 && mouseY<335 && mouseY>285) {
    flower1rotate();
} else {
    flower1();
}
  pop();

//leaves flower 1
if (mouseX<438 && mouseX>388 && mouseY<335 && mouseY>285) {
  push();
    flower1leafmove();
  pop();
} else {
  push();
    flower1leaf();
  pop();
}

//flower 2
if (mouseX<543 && mouseX>483 && mouseY<290 && mouseY>230) {
    flower2rotate();
} else {
    flower2();
}

//leaves flower 2
  push();
if (mouseX<543 && mouseX>483 && mouseY<290 && mouseY>230) {
    flower2leafmove();
} else {
    flower2leaf();
}
  pop();

//grass
if (mouseX<640 && mouseX>0 && mouseY<480 && mouseY>450) {
    grassmove();
} else {
    grass();
}

//sun + sunbeams
if (mouseX<180 && mouseX>80 && mouseY<170 && mouseY>70) {
    sunbeamsrotate();
} else {
    sunbeams();
}
}

function flower1rotate(){
    translate(width/3+200, height/3+150);
    rotate(radians(frameCount)); //making it rotate
    for (var i = 0; i < 8; i++) { //for-loop
  push();
    rotate(TWO_PI * i / 8); //making 8 ellipses in a circle
    fill(213, 51, 249)
    stroke(213, 51, 249);
    strokeWeight(3)
    ellipse(20, 20, 20, 20);
  pop();
}
  pop();
    fill(255, 145, 4);
    noStroke();
    ellipse(413, 310, 50, 50); //ellipse in center
}

function flower1(){
    translate(width/3+200, height/3+150);
    for (var i = 0; i < 18; i++) { //for-loop
  push();
    rotate(TWO_PI * i / 8); //making 8 ellipses in a circle
    fill(213, 51, 249)
    stroke(213, 51, 249);
    strokeWeight(3)
    ellipse(20, 20, 20, 20);
  pop();
}
  pop();
    fill(255, 145, 4);
    noStroke();
    ellipse(413, 310, 50, 50);
}

function flower1leaf(){
    stroke(44, 157, 77);
    strokeWeight(5);
    line (413, 350, 413, 540);

    translate(width / 2, height / 2);
    fill(44, 157, 77);
    ellipse(65, 175, 50, 20);

    fill(44, 157, 77);
    ellipse(120, 175, 50, 20);
}

function flower1leafmove(){
    stroke(44, 157, 77);
    strokeWeight(5);
    line (413, 350, 413, 540);

  push();
    translate(width / 2, height / 2);
    rotate(PI / 10);
    fill(44, 157, 77);
    ellipse(115, 140, 50, 20);
  pop();

  push();
    translate(width / 2, height / 2);
    rotate(PI/-10);
    fill(44, 157, 77);
    ellipse(62, 197, 50, 20);
  pop();

}

function flower2rotate(){
  push();
    translate(width/3+300, height/3+100);
    rotate(radians(frameCount)); //making it rotate
    for (var i = 0; i < 8; i++) { //for-loop
  push();
    rotate(TWO_PI * i / 8); //making 8 ellipses in a circle
    fill(249, 51, 141)
    stroke(249, 51, 141);
    strokeWeight(3)
    ellipse(25, 25, 25, 25);
  pop();
}
  pop();
    fill(255, 145, 4);
    noStroke();
    ellipse(513, 260, 60, 60); //ellipse in center
}

function flower2(){
  push();
    translate(width/3+300, height/3+100);
    for (var i = 0; i < 8; i++) { //for-loop
  push();
    rotate(TWO_PI * i / 8); //making 8 ellipses in a circle
    fill(249, 51, 141)
    stroke(249, 51, 141);
    strokeWeight(3)
    ellipse(25, 25, 25, 25);
  pop();
}
  pop();
    fill(255, 145, 4);
    noStroke();
    ellipse(513, 260, 60, 60); //ellipse in center
}

function flower2leaf(){
    stroke(44, 157, 77);
    strokeWeight(5);
    line (510, 310, 510, 500);

    translate(width / 2, height / 2);
    fill(44, 157, 77);
    ellipse(163, 123, 50, 20);

    fill(44, 157, 77);
    ellipse(215, 123, 50, 20);
}

function flower2leafmove(){
    stroke(44, 157, 77);
    strokeWeight(5);
    line (510, 310, 510, 500);

  push();
    translate(width / 2, height / 2);
    rotate(PI / 10);
    fill(44, 157, 77);
    ellipse(190, 60, 50, 20);
  pop();

  push();
    translate(width / 2, height / 2);
    rotate(PI/-10);
    fill(44, 157, 77);
    ellipse(170, 177, 50, 20);
  pop();
}

function grassmove(){
  push();
    for (var x=0; x<=width; x=x+10) { //for-loop
    stroke(44, 157, 77);
    strokeWeight(5);
    line (x, 480, x, 440);
}
    for (var x=0; x<=width; x=x+10) { //for-loop
    stroke(44, 157, 77);
    strokeWeight(4);
    line (5+x, 480, 5+x, 450);
}
  pop();
}

function grass(){
  push();
    for (var x=0; x<=width; x=x+10) { //for-loop
    stroke(44, 157, 77);
    strokeWeight(5);
    line (x, 480, x, 450);
}
    for (var x=0; x<=width; x=x+10) { //for-loop
    stroke(44, 157, 77);
    strokeWeight(4);
    line (5+x, 480, 5+x, 460);
}
  pop();
}

function sunbeamsrotate (){
    stroke(255, 204, 0);
    strokeWeight(4);
    fill(255, 204, 0);
    circle(130, 120, 100); //sun
  push();
    translate(windowWidth/5, windowHeight/6);
    rotate(radians(frameCount)); //making the sunbeams rotate
    //8
    for (var i = 0; i < 13; i++) { //for-loop
    rotate(TWO_PI * i /8); //8 sunbeams
    fill(255, 243, 0)
    stroke(255, 243, 0);
    strokeWeight(3)
    line(50,50,80,80);
}
  pop();
}

function sunbeams(){
    stroke(255, 204, 0);
    strokeWeight(4);
    fill(255, 204, 0);
    circle(130, 120, 100); //sun
  push();
    translate(windowWidth/5, windowHeight/6);
    for (var i = 0; i < 8; i++) { //for-loop
  push();
    rotate(TWO_PI * i / 8); //8 sunbeams
    fill(255, 243, 0)
    stroke(255, 243, 0);
    strokeWeight(3)
    line(50,50,80,80);
  pop();
}
  pop();
}

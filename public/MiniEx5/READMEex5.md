MiniEx5

![ScreenShot](screenshot.png)

https://stinemygind.gitlab.io/ap2020/MiniEx5 
I have been trying almost everything, and I can’t figure out how to make my URL work, so you will have to put the runme into Atom - sorry ://

https://gitlab.com/StineMygind/ap2020/-/blob/master/public/MiniEx5/RUMMEex5.js

For this miniEx I have been focusing on trying all the things we have learned in aesthetic programming and that I haven’t used for the other miniEx’s. This included experimenting with for loops, rotations and using the if-else statement and &&.
When I had to choose which exercise I wanted to redo, I choose the first miniEx, because it looked very poor. So my choice is based on how I could implement a lot of syntaxes I haven’t been using before and not on reflections from the readings. However, it is still possible for me to reflect upon my creations in relation to the readings.
First of all, I will start by introducing my sketch. It looks like some kind of childish drawing because it consists of a blue background, which simulates the sky, and grass which is made by two for loops with lines in two different sizes. In the corner to left is a sun with sunbeams which is also made by the for loop and if you place the mouse on the sun the sunbeams will move and rotate around the sun. I have also made two flowers where the leaves move when the mouse touches the flower. So I have tried to use syntax that I haven’t used before and furthermore tried to mix some of them. 

Reflection on Aesthetic Programming

The progress of my two sketches shows which development my programming skills have been going through the recent weeks. This makes me think of how learning to program gives the programmer new possibilities. I think that programming as a skill can make the programmer more powerful because the programmer is able to make something that only the ones with the programming skill can do. I also think that it improves other things like the skill to design and understand other kinds of software.  
As the reading also addresses I think that aesthetic programming not only consists of learning to program but also gives the student some kind of context to the field of programming. The course affords not only basic knowledge but also reflection, which it seeks through literature that discusses appropriate topics and gives the student knowledge about the field of software. Digital culture exists because of the coding practice and they affect each other either way.
I think that this quote, from the Preface of Aesthetic Programming: A Handbook of Software Studies by Soon and Cox, gives a good impression of why we learn aesthetic programming: “Computational culture (…) a means to engage with programming as a way to question existing technological interfaces and further create changes in the technical system. Thus we consider programming to be a dynamic cultural practice and phenomenon, a way of thinking and doing in the world, and a means to understand some of the complex procedures that underwrite our lived realities, in order to act upon those realities.”. I like the quote because it opens up the field of computational culture but at the time it makes the field a bit complex.
I consider aesthetic programming a good an relevant knowledge for my future because I not only get introduced to the concepts of programming, but I also get an impression of how it can change the world in both small and big scale. 
